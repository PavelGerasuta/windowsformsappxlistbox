﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsAppXListBox2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            xListBox21.OnItemSelect += new EventHandler(textBoxConclusion_TextChanged);
        }

        private void buttonAddParam_Click(object sender, EventArgs e)
        {
            
            xListBox21.AddParam("item");
        }

        private void xListBox21_Click(object sender, EventArgs e)
        {

        }

        private void buttonDeleteLastItem_Click(object sender, EventArgs e)
        {
            
            xListBox21.DeleteParam(xListBox21.m_Count - 1);
            
            
        }

        private void buttonCleatXListBox_Click(object sender, EventArgs e)
        {
            xListBox21.ClearParam();
        }

        private void textBoxConclusion_TextChanged(object sender, EventArgs e)
        {
            textBoxConclusion.Text =$"item{Convert.ToString(xListBox21.now_Point)}";
        }

        private void but_Click(object sender, EventArgs e)
        {
           // MessageBox.Show(Convert.ToString(xListBox21.now_Point));
        }


    }
}
