﻿
namespace WindowsFormsAppXListBox2
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAddParam = new System.Windows.Forms.Button();
            this.buttonDeleteLastItem = new System.Windows.Forms.Button();
            this.buttonCleatXListBox = new System.Windows.Forms.Button();
            this.textBoxConclusion = new System.Windows.Forms.TextBox();
            this.xListBox21 = new WindowsFormsAppXListBox2.XListBox2();
            this.SuspendLayout();
            // 
            // buttonAddParam
            // 
            this.buttonAddParam.Location = new System.Drawing.Point(203, 47);
            this.buttonAddParam.Name = "buttonAddParam";
            this.buttonAddParam.Size = new System.Drawing.Size(75, 23);
            this.buttonAddParam.TabIndex = 1;
            this.buttonAddParam.Text = "Добавить";
            this.buttonAddParam.UseVisualStyleBackColor = true;
            this.buttonAddParam.Click += new System.EventHandler(this.buttonAddParam_Click);
            // 
            // buttonDeleteLastItem
            // 
            this.buttonDeleteLastItem.Location = new System.Drawing.Point(203, 86);
            this.buttonDeleteLastItem.Name = "buttonDeleteLastItem";
            this.buttonDeleteLastItem.Size = new System.Drawing.Size(75, 23);
            this.buttonDeleteLastItem.TabIndex = 2;
            this.buttonDeleteLastItem.Text = "Удалить";
            this.buttonDeleteLastItem.UseVisualStyleBackColor = true;
            this.buttonDeleteLastItem.Click += new System.EventHandler(this.buttonDeleteLastItem_Click);
            // 
            // buttonCleatXListBox
            // 
            this.buttonCleatXListBox.Location = new System.Drawing.Point(203, 128);
            this.buttonCleatXListBox.Name = "buttonCleatXListBox";
            this.buttonCleatXListBox.Size = new System.Drawing.Size(75, 23);
            this.buttonCleatXListBox.TabIndex = 3;
            this.buttonCleatXListBox.Text = "Очистить";
            this.buttonCleatXListBox.UseVisualStyleBackColor = true;
            this.buttonCleatXListBox.Click += new System.EventHandler(this.buttonCleatXListBox_Click);
            // 
            // textBoxConclusion
            // 
            this.textBoxConclusion.Location = new System.Drawing.Point(303, 47);
            this.textBoxConclusion.Name = "textBoxConclusion";
            this.textBoxConclusion.ReadOnly = true;
            this.textBoxConclusion.Size = new System.Drawing.Size(100, 20);
            this.textBoxConclusion.TabIndex = 4;
            this.textBoxConclusion.TextChanged += new System.EventHandler(this.textBoxConclusion_TextChanged);
            // 
            // xListBox21
            // 
            this.xListBox21.Location = new System.Drawing.Point(48, 47);
            this.xListBox21.m_Count = 4;
            this.xListBox21.m_Max = 13;
            this.xListBox21.m_Min = 0;
            this.xListBox21.Name = "xListBox21";
            this.xListBox21.now_Point = 0;
            this.xListBox21.Size = new System.Drawing.Size(111, 176);
            this.xListBox21.TabIndex = 0;
            this.xListBox21.Text = "xListBox21";
            this.xListBox21.Click += new System.EventHandler(this.xListBox21_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(436, 271);
            this.Controls.Add(this.textBoxConclusion);
            this.Controls.Add(this.buttonCleatXListBox);
            this.Controls.Add(this.buttonDeleteLastItem);
            this.Controls.Add(this.buttonAddParam);
            this.Controls.Add(this.xListBox21);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private XListBox2 xListBox21;
        private System.Windows.Forms.Button buttonAddParam;
        private System.Windows.Forms.Button buttonDeleteLastItem;
        private System.Windows.Forms.Button buttonCleatXListBox;
        private System.Windows.Forms.TextBox textBoxConclusion;
    }
}

