﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsAppXListBox2
{
    public class XListBox2 : Control
    {
        private int Ypoint;

        private bool Flag;

        private double d_y;

        public int m_Count { get; set; }

        public int m_Min { get; set; }

        public int m_Max { get; set; }

        public int now_Point { get; set; }

        private List<string> _Params = new List<string>();

        private EventHandler EventItemSelect;


        //Методы
        public void AddParam(string param)
        {
            if (_Params.Count()<= m_Max)
            {
                _Params.Add(param);
                m_Count = _Params.Count();
                Refresh();
            }
            
        }

        public void DeleteParam(int index)
        {
            if (m_Count!=0)
            {
                _Params.RemoveAt(index);
                m_Count = _Params.Count();
                if (index == d_y)
                {
                    Flag = false;
                }
                Refresh();
            }
           
        }

        public void ClearParam()
        {
            _Params.Clear();
            m_Count = _Params.Count();
            Flag = false;
            Refresh();
        }

        public XListBox2()
        {
            m_Count = 4;
            m_Max = 4;
            for (int i = 0; i < m_Count; i++)
                _Params.Add("item");
            Refresh();
        }

        protected void Do()
        {
            EventItemSelect?.Invoke(this, EventArgs.Empty);
        }

        

        public event EventHandler OnItemSelect
        {
            add { EventItemSelect += value; }
            remove { EventItemSelect -= value; }
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);

            if ((e.Y > 0) & (e.Y < (m_Count) * 12) & (e.X < Width ) & (e.X > 0))
            {
                Flag = true;
                Ypoint = e.Y;
                Refresh();
                Do();

            }

        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            Graphics g = e.Graphics;

            g.DrawRectangle(new Pen(Brushes.Black, 2), 0, 0, Width, Height);

            m_Max = (Height / 12) -1;


            int k = 0;
            

            
                for (int i = 0; i < m_Count; i++)
                {
                    g.DrawString(_Params[i]+i, new Font("Times New Roman", 10),
                       Brushes.Black, 0, k);
                    k += 12;
                }

                if (Flag == true)
                {
                    int i_count_now = m_Count * (k);
                    d_y = Ypoint / 12;

                    if ((Ypoint < i_count_now) & (Ypoint > 0) & (d_y <= m_Count))
                    {

                        //g.DrawRectangle(new Pen(Brushes.Black, 1), 0, (int)d_y * 12 + 2, Width, 13);
                        

                        g.FillRectangle(new SolidBrush(Color.FromArgb(50, 80, 80, 80)), 0, (int)d_y * 12 + 2, Width, 13);

                        now_Point = (int)d_y;

                }

                }
            




        }

    }
}
